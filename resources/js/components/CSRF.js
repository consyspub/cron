export default function CSRF() {
    return document.head.querySelector("meta[name=csrf-token]").getAttribute("content");
}