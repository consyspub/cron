import ReactDOM from 'react-dom';
import { render } from "react-dom";
import {
    HashRouter,
    BrowserRouter,
    Routes,
    Route,
    Outlet
} from "react-router-dom";

import Index from "./Index";
import Schedules from "./Schedules";
import Schedule from "./Schedule";

if (document.getElementById('root')) {
    ReactDOM.render(
        <HashRouter>
            <Routes>
                <Route path="/" element={<Index />}>
                    <Route index element={<Schedules />} />
                    <Route path=":id" element={<Schedule />} />
                    <Route path="new" element={<Schedule />} />
                </Route>
            </Routes>
        </HashRouter>,
        document.getElementById('root')
    );
}