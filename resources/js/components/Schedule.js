import React from 'react';
import {
    Button,
    Form,
    Row,
    Col,
    Modal,
    Accordion
} from 'react-bootstrap';
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import CSRF from './CSRF';

export default function Schedule() {
    let navigate = useNavigate();
    let { id } = useParams();

    const [loaded, setLoaded] = React.useState(false);
    const [description, setDescription] = React.useState("");
    const [schedule, setSchedule] = React.useState("");
    const [action, setAction] = React.useState("");
    const [actions, setActions] = React.useState([]);
    const [showDialogSave, setShowDialogSave] = React.useState(false);
    const [showDialogRemove, setShowDialogRemove] = React.useState(false);
    const [showDialogAction, setShowDialogAction] = React.useState(false);

    const cronOptions = ["*", 1,2,3,4,5,6,7,8,9,0];

    React.useEffect(() => {
        fetch =  async () => {
            let response = await axios.get("./schedules/" + id);
            if (response.data.description) {
                setDescription(response.data.description);
            }
            
            if (response.data.schedule) {
                setSchedule(response.data.schedule);
            }
            
            if (response.data.actions) {
                let _actions = response.data.actions;
                let actionSet = [];
                setActions([]);
                _actions.map((_entry) => {
                    actionSet.push(_entry.action);
                });

                setActions(actionSet);
            }

            //console.log("actions loaded");
            //console.log(actions);
            
            setLoaded(true);
        }
        if (id) {
            fetch();
        }
        
    }, [loaded]);

    const handleCloseDialogSave = () => setShowDialogSave(false);
    const handleShowDialogSave = () => setShowDialogSave(true);
    const handleCloseDialogRemove = () => setShowDialogRemove(false);
    const handleShowDialogRemove = () => setShowDialogRemove(true);
    const handleCloseDialogAction = () => setShowDialogAction(false);
    const handleShowDialogAction = () => {
        setAction("");
        setShowDialogAction(true);
    }

    const save = () => {
        handleCloseDialogSave();

        if (id) {
            axios.put('./schedules/' + id, {
                _token : CSRF(),
                description: description,
                schedule: schedule,
                actions : actions
            })
            .then(function (response) {
                NotificationManager.success('Agendamento realizado com sucesso', 'Schedule');
            })
            .catch(function (err) {
                NotificationManager.error('Não foi possível concluir sua solicitação', 'Schedule');
            });
        }
        else {
            axios.post('./schedules', {
                _token : CSRF(),
                description: description,
                schedule: schedule,
                actions : actions
            })
            .then(function (response) {
                NotificationManager.success('Agendamento realizado com sucesso', 'Schedule');
            })
            .catch(function (err) {
                NotificationManager.error('Não foi possível concluir sua solicitação', 'Schedule');
            });
        }
        
    }
    
    const remove = () => {
        handleCloseDialogRemove();
        
        axios.delete('./schedules/' + id, {
            _token : CSRF(),
        })
        .then(function (response) {
            NotificationManager.success('Agendamento removido com sucesso', 'Schedule');
            navigate("/");
        })
        .catch(function (err) {
            NotificationManager.error('Não foi possível concluir sua solicitação', 'Schedule');
        });
    }

    const back = () => {
        navigate("/");
    }

    const addAction = () => {
        actions.push(action);
        handleCloseDialogAction();
    }

    const removeAction = (index) => {
        console.log('removeAction');
        console.log(actions);

        let _actions = [];

        let i = 0;
        actions.map((entry) => {
            if (i != index) {
                _actions.push(entry);
            }
            i++;
        });

        console.log(_actions);
        console.log(index);

        setActions(_actions);
    }    

    let htmlActions = "";
    if (actions) {
        let index = 0;
        htmlActions = actions.map(
            (_entry) => {
                let key = index+1;
                let _index = index;
                index++;

                return (
                    <Accordion.Item eventKey={key.toString()}>
                        <Accordion.Header>
                                    {index} - Ação
                        </Accordion.Header>
                        <Accordion.Body>                            
                            <Row>
                                <Col>
                                    {_entry}
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Button variant="primary" onClick={(e) => removeAction(_index)}>Remover</Button>
                                </Col>
                            </Row>
                        </Accordion.Body>
                    </Accordion.Item>
                );
            }
            
        );
    }

    console.log(actions);

    return (
        <>
            <h1>Schedule</h1>
            <Row>
                <Col>
                    <Form.Label>Descrição</Form.Label>
                    <Form.Control type="text" value={description} onChange={(event) => setDescription(event.target.value)} />
                </Col>
            </Row>
            <Row>
                <Col>
                    <Form.Label>Script de agendamento</Form.Label>
                    <Form.Control type="text" value={schedule} onChange={(event) => setSchedule(event.target.value)} />
                    <Form.Text id="passwordHelpBlock">
                        *       *       *       *       *
                        minuto  hora    dia     mês     dia da semana
                    </Form.Text>
                </Col>
            </Row>
            <Row>
                <Col>
                    <h3>Ações</h3>
                </Col>
                <Col>
                    <Button variant="info" onClick={handleShowDialogAction}>Adicionar</Button>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Accordion defaultActiveKey="0">
                        {htmlActions}
                    </Accordion>
                </Col>
            </Row>
            <Row>
                <Col style={{height:"30px"}}></Col>
            </Row>
            <Row>
                <Col>
                    <Button variant="info" onClick={back}>Voltar</Button>
                </Col>
                <Col>
                    <Button variant="success" onClick={handleShowDialogSave}>Salvar</Button>
                </Col>
                <Col>
                {
                    id ? <Button variant="danger" onClick={handleShowDialogRemove}>Excluir</Button> : ""
                }
                </Col>
            </Row>

            <Modal show={showDialogSave} onHide={handleCloseDialogSave}>
                <Modal.Header closeButton>
                    <Modal.Title>Schedule</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <p>Confirma que deseja salvar estas informações?</p>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseDialogSave}>Fechar</Button>
                    <Button variant="primary" onClick={save}>Confirmar</Button>
                </Modal.Footer>
            </Modal>

            <Modal show={showDialogRemove} onHide={handleCloseDialogRemove}>
                <Modal.Header closeButton>
                    <Modal.Title>Schedule</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <p>Confirma que deseja remover este agendamento?</p>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseDialogRemove}>Fechar</Button>
                    <Button variant="primary" onClick={remove}>Confirmar</Button>
                </Modal.Footer>
            </Modal>
            
            <Modal show={showDialogAction} onHide={handleCloseDialogAction}>
                <Modal.Header closeButton>
                    <Modal.Title>Ação</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <p><Form.Label>Script de agendamento</Form.Label>
                    <Form.Control type="text" value={action} onChange={(event) => setAction(event.target.value)} /></p>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseDialogAction}>Fechar</Button>
                    <Button variant="primary" onClick={addAction}>Adicionar</Button>
                </Modal.Footer>
            </Modal>

            <NotificationContainer/>
        </>
    );
}