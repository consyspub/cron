import React from 'react';
import {
    Button,
    Row,
    Col
} from 'react-bootstrap';
import { useNavigate, Link } from "react-router-dom";
import DataTable from 'react-data-table-component';
import axios from 'axios';

export default function Schedules() {
    let navigate = useNavigate();

    const [loaded, setLoaded] = React.useState(false);
    const [data, setData] = React.useState([]);

    React.useEffect(() => {
        fetch =  async () => {
            let response = await axios.get("./schedules");
            setData(response.data);
            setLoaded(true);
        }
        fetch();
    }, [loaded]);

    const openNew = () => {
        navigate('/new');
    }

    const columns = [
        {
            name: 'Descrição',
            selector: row => row.description,
        },
        {
            name: '',
            selector: row => <Link to={"./" + row.id}>Abrir</Link>,
        },
    ];
    

    return (
        <>            
            <Row>
                <Col>
                    <h1>Agendamentos</h1>
                </Col>
                <Col>
                    <Button variant="success" onClick={openNew}>Novo</Button>
                </Col>
            </Row>
            <Row>
                <Col>
                    <DataTable
                        columns={columns}
                        data={data}
                        noDataComponent="Nenhum registro encontrado"
                        paginationComponentOptions={{
                            rowsPerPageText: 'Linhas por página:', 
                            rangeSeparatorText: 'de', 
                            noRowsPerPage: false, 
                            selectAllRowsItem: false, 
                            selectAllRowsItemText: 'Todas'
                        }}
                    />
                </Col>
            </Row>
        </>
    );
}