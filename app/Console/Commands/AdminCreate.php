<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class AdminCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create admin user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->ask('Qual o seu nome?');
        $email = $this->ask('Qual o seu e-mail?');
        $password = $this->secret('Qual a senha deseja definir para o usuário?');

        $user = new \App\Models\User();

        $user->name = $name;
        $user->email = $email;
        $user->password = Hash::make($password);
        if ($user->save()) {
            $this->info('Admin created succesfully');
        }
        else {
            $this->error('An error ocurred');
        }

        return 0;
    }
}