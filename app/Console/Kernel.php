<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Models\TaskScheduler;
use App\Models\TaskSchedule;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        // Load all task schedulers
        $tasks = TaskScheduler::all()->load(["actions"]);

        foreach ($tasks as $task) {
            foreach ($task->actions as $action) {
                if (\Cron\CronExpression::isValidExpression($task->schedule)) {
                    try {
                        $schedule->exec($action->action)->cron($task->schedule);

                        // Store task successfully execution
                        TaskSchedule::create([
                            "task_scheduler_id" => $task->id
                        ]);
                    }
                    catch (\Exception $err) {
    
                    }
                }                
            }            
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
