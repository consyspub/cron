<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TaskScheduler;
use App\Models\TaskAction;

class TaskSchedulerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(TaskScheduler::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $TaskScheduler = new TaskScheduler();
        $TaskScheduler->description = $request->description;
        $TaskScheduler->schedule = $request->schedule;        

        if ($TaskScheduler->save()) {
            if ($request->actions) {
                TaskAction::where("task_scheduler_id", $TaskScheduler->id)->delete();

                foreach ($request->actions as $action) {
                    TaskAction::create([
                        "task_scheduler_id" => $TaskScheduler->id,
                        "action" => $action
                    ]);
                }
            }

            return response('ok', 200);
        }

        return response('', 500);        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(TaskScheduler::find($id)->load(["actions"]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $TaskScheduler = TaskScheduler::find($id);
        $TaskScheduler->description = $request->description;
        $TaskScheduler->schedule = $request->schedule;        

        if ($TaskScheduler->save()) {
            if ($request->actions) {
                TaskAction::where("task_scheduler_id", $TaskScheduler->id)->delete();

                foreach ($request->actions as $action) {
                    TaskAction::create([
                        "task_scheduler_id" => $TaskScheduler->id,
                        "action" => $action
                    ]);
                }
            }

            return response('ok', 200);
        }

        return response('', 500);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {            
            TaskAction::where("task_scheduler_id", $id)->delete();
            TaskScheduler::find($id)->delete();
            
            return response('ok', 200);
        }
        catch (\Exception $err) {

        }        

        return response('', 500);
    }
}
