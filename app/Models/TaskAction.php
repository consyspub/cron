<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskAction extends Model
{
    use HasFactory;

    protected $table = "task_action";

    protected $fillable = [
        "task_scheduler_id",
        "action"
    ];
}
