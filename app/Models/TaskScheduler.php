<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskScheduler extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "task_scheduler";

    public function actions() {
        return $this->hasMany(TaskAction::class);
    }
}
